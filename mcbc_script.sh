#!/bin/bash


# Переменные
SOURCE=/dev/sda
RESTOREDISK=`lsblk -flp | grep DATA | awk '{print $1}'` #ищем среди дисков файловую систему с тегом "DATA"
DESTINATION="/mnt"
IMG_NAME="sdatest.ddimg.gz"

#----------Создаём массив с данными lsblk-------------
DISKLIST=()
h=0
for j in `lsblk -npl -o 'NAME','SIZE'`
do 
	DISKLIST[h]="${j}" 
	((h+=2))
done

#--------------------------------------------------------------------------------------------------------

# Функция создания образа с псевдографикой
function f_backup {
(pv -n ${SOURCE} | gzip -1 > ${DESTINATION}/${IMG_NAME} ) 2>&1 | whiptail --title "Создание образа восстановления" --gauge "Пожалуйста, подождите..." 10 70 0
}

# Функция восстановаления образа с псевдографикой
function f_restore {
(pv -n $IMG_LOCATION | gunzip | dd of=${REST_DESTINATION} bs=4k) 2>&1 | whiptail --title "Восстановление системы из образа" --gauge "Пожалуйста, подождите..." 10 70 0
}	

# Подтверждение завершения задачи
function f_ok {
whiptail --msgbox "Задача выполнена. Нажмите OK для перезагрузки" 10 70
sync && exit #заменить exit на reboot
}

#------------------------------------------------------------------------------------------------------------
function f_mode {
MODE=$(whiptail --menu "Выберите необходимый режим работы:" 16 64 8\
	"1" "Исползовать этот накопитель" \
	"2" "Использовать выделенный раздел на диске" 3>&1 1>&2 2>&3)

if [ "$MODE" = "1" ]; then
	umount /mnt
	mount ${RESTOREDISK} /mnt
	DESTINATION="/mnt"
	f_choice

elif [ "$MODE" = "2" ]; then
	umount /mnt
	#Создать возможность выбора раздела для сохранения
	DISK=$(whiptail --menu "Выберите диск для хранения образа" 20 80 10 "${DISKLIST[@]}" 3>&1 1>&2 2>&3)
	mount ${DISK} /mnt
	#echo ${DISK}
	#f_ok
	f_choice

else
	f_ok
fi
}

function f_choice {

CHOICE=$(whiptail  --menu "Выберите необходимую задачу" 16 64 8\
	"BACKUP" "Создание образа диска" \
	"RESTORE" "Восстановление диска из образа" \
	"EXIT" "Выход в главное меню" 3>&1 1>&2 2>&3)

if [ -z "$CHOICE" ]; then
	echo "No option chosen"

elif [ "$CHOICE" = "BACKUP" ]; then
        
	SOURCE=$(whiptail --menu "Выберите диск для которого нужно создать образ" 20 78 10 "${DISKLIST[@]}" 3>&1 1>&2 2>&3)
	#IMG_NAME="`date +%F.%H-%M``echo $SOURCE | sed -e 's=/=_=g'`.gz" 
	IMG_NAME="backup`echo $SOURCE | sed -e 's=/=_=g'`.gz"
	f_backup      
        f_ok	

elif [ "$CHOICE" = "RESTORE" ]; then 
	
	REST_DESTINATION=$(whiptail --menu "Выберите диск для восстановления из образа " 20 78 10 "${DISKLIST[@]}" 3>&1 1>&2 2>&3)
	IMG_NAME="backup`echo $REST_DESTINATION | sed -e 's=/=_=g'`.gz"
	IMG_LOCATION="/mnt/${IMG_NAME}" 
	echo $REST_DESTINATION
	echo $IMG_LOCATION
	f_restore
	f_ok

elif [ "$CHOICE" = "EXIT"  ]; then 
	f_mode

else
	f_ok
fi

}

f_mode
#whiptail --title "COMPLETE" --msgbox "TO EXIT press OK" 10 70



